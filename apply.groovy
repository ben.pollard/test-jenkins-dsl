freeStyleJob('apply-dsl') {
    displayName('apply-dsl')
    description('Applies all the Jenkins DSLs in the repository.')

    checkoutRetryCount(3)

    scm {
        git {
            remote {
                url('https://gitlab.com/ben.pollard/test-jenkins-dsl.git')
            }
            branches('*/master')
            extensions {
                wipeOutWorkspace()
                cleanAfterCheckout()
            }
        }
    }

    wrappers { colorizeOutput() }

    steps {
        dsl {
            external('**/*.groovy')
            removeAction('DELETE')
            removeViewAction('DELETE')
        }
    }
}
